package maputil

// MergeStringMaps - Merges `source` map into `dest` and returns it.
func MergeStringMaps(dest, source map[string]string) map[string]string {
	for key, value := range source {
		dest[key] = value
	}
	return dest
}
